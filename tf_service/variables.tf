variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-1"
}

variable "network_state_s3_bucket" {}

variable "dev_sshpubkey_file" {}
variable "prod_sshpubkey_file" {}
