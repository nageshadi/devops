variable "bucket_name" {}

variable "acl" {
  default = "private"
}

variable "objects_to_upload" {
  type    = "list"
  default = []
}
