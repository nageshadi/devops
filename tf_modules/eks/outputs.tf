output "kubeconfig" {
  value = "${local.kubeconfig}"
}

output "worker_role_arn" {
  value = "${aws_iam_role.eks-worker.arn}"
}

output "eks_worker_sg_id" {
  value = "${aws_security_group.eks-worker.id}"
}

output "eks_cluster_arn" {
  value = "${aws_eks_cluster.eks-cluster.arn}"
}
